<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
<title>Complot</title>
<link rel="shortcut icon" href="logo.png">
<link rel="stylesheet" href="../style.css">
</head>

<body>
<h1>Complot</h1>
<hr>
<par>
<a href="https://gitlab.maths.lth.se/frejd/complot/">complot.h</a>
is a single-header simple complex number plotting library for the C programming
language. It outputs <a href="http://tools.suckless.org/farbfeld/">farbfeld</a>
images from arrays of complex numbers. But also has two convenience functions
to make plotting a breeze, see the code example.
</par>
<br>
<div class="imgrow">
<img src="log.png">
<img src="exp.png">
<img src="tan.png">
<img src="mobiustransform.png">
</div>
<par>
Above is, in order of appearance, the logarithm, the exponential, the
tangent, and the Möbius transform on the unit square.
</par>
<h2>Method</h2>
<hr>
<par>
Complot uses so-called domain coloring to compensate for the fact that
the complex-plane is two-dimensional. It can therefore not be plotted
by standard methods since the domain is two-dimensional as well, which
leaves us with four-dimensions, too much too plot regularly.
<br><br>
Usually one maps the complex argument to the hue of the color and
optionally maps the modulus (absolute value) to saturation and/or value.
For example one could map large absolute values to black and those around
zero to white. Another option is to map the modolus discontinuosly to
the saturation-value space. This is usually done to indicate contour lines,
for example, paint a black strip when you pass every power of e. That is
if x is close to 1, e, e², ..., then map those to a value of zero: black.
<br><br>
Most colorings use a one-to-one map of the argument to the hue. This leaves
us with smooth gradients, which in my opinion make it harder to tell what
is going on. Therefore I have opted to split the the argument into bands
of different hue (see examples below.) I have also decided to go with the
method of painting contour lines instead of turning black/white, this
leaves us with some pretty fractal-like images in some cases.
</par>
<h2>Examples</h2>
<hr>
<img src="expinv_bc.png" align="right" style="padding-left:8px;">
<par>
Here is the ubiquitous exp(1/z) which has an essential singularity at zero.
It is this singularity that makes the argument change "infinitely" close
to zero. Note that the color bands keep changing when the values
get closer to zero. Also note the black bands, as stated above these
indicate growth of the function. On the right-half-plane the function diverges
as z -> 0. But not on the left-half-plane. I think the bands exemplify
clearly the argument changing even at low resolution pictures, this one is
only 256x256 pixels. Compare with the gradient-coloring below.
</par>

<h3>Code</h3>
Below follows a small example that generates the image above.
<code><pre>
/* cexpinvplot.c: plots e^(1/z) on the unit square */
#include &lt;complex.h&gt;

#include "complot.h"

#define DIM 256

complex double
cexpinv(complex double z)
{
	return cexp(1 / z);
}

int
main()
{
	/* plot cexpinv on the unit square to stdout */
	complot(cexpinv, DIM, BANDED_CONTOUR, stdout);
}
</pre></code>
Compile to <code>cexpinvplot</code> and in a shell redirect its output to
an image of farbfeld format.
<code><pre>
$ ./cexpinvplot > cexpinv.ff
</pre></code>
Optionally one can of course convert this to png or other formats.
<code><pre>$ ff2png < cexpinv.ff > cexpinv.png</pre></code>
Or directly pipe it through.
<code><pre>$ ./cexpinvplot | ff2png > cexpinv.png</pre></code>

<h2>Colorings</h2>
<hr>
<img src="expinv_sr.png" align="right" style="padding-left:8px;">
<par>
A coloring is defined to be a map from the complex plane into the
red-green-blue color space or even the rgb-alpha space if transparency
is asked for. My coloring with bands and black contour log-bands has
its advantages and has a certain 'soul' to them (just look at the examples!)
But perhaps more canonical is a coloring that is continuous and hence one
that does not have banding. To create such a coloring we remove the banding
above, and map the argument directly to the hue. Then what remains is to
continously map the modulus into the saturation-value space or the 
saturation-lightness space. Here one maps white to zero, black to infinity, or
one can switch these (simply pick one.) Usually one fixes the saturation
to the maximum, so then you map the modulus to the value in other words the
absolute value to the value (HA!). Disregarding this, it is more common to work
with lightness rather than value. This is what I have done. Lightness is mapped
to the function x / (1 + x), where x is the square root of the modulus. This
generates the image above.
</par>
<br><br>
<par>
You can access the four builtin colorings in complot via the fourth argument. Use
the macros <code>BANDED</code>, <code>CONTOUR</code>, <code>SMOOTH</code>,
or <code>RIEMANN</code> to get banded, banded with black contours, smooth,
and the canonical coloring described above, respectively. So for a smooth image
with infinity mapped to black and zero mapped to white, one would call the function
as
<code><pre>complot(vals, width, height, SMOOTH_RIEMANN, stream);</pre></code>
</par>

<h3>Comparisons</h3>
<par>
Here are come comparisons between the banded and smooth colorings.
</par>

<div class="imgrow">
<img src="pow_b.gif">
<img src="pow_c.gif">
<img src="pow_s.gif">
<img src="pow_r.gif">
</div>
<par>
First of are these gifs that animate z^w where w pass through different
roots of unity. Here the black bands are actually the z's that map z^w
close to modulus one. While the banded version is more interesting it is a bit
jarring to look at.
</par>
<div class="imgrow">
<img src="randbp_b.png">
<img src="randbp_c.png">
<img src="randbp_s.png">
<img src="randbp_r.png">
</div>
<par>
Secondly, a Blaschke product with 16 random zeroes in the unit square.
One can identify the Zeroes as the points where the color shifts through
the whole spectrum. My favorite is the first picture, banded without
contours.
</par>

<div class="imgrow">
<img src="disktoplane_b.png">
<img src="disktoplane_c.png">
<img src="disktoplane_s.png">
<img src="disktoplane_r.png">
</div>
<par>
Lastly let's check out the function that takes z to z / (1 - |z|^2), where
|z| is the modulus of z. This maps the unit disk to the complex plane 
bijectively and the function is continuous, but it is not comformal (that is
impossible.)
</par>

<h2>Resources and links</h2>
<hr>
<par>
Here are some links that I found useful while making these. I especially 
recommend browsing the first.
<br>
<ol>
<li><a href="http://locsi.web.elte.hu/en/complexdemo/">Colorful visualization of complex functions</a></li>
<li><a href="http://en.wikipedia.org/wiki/Domain_coloring">Domain coloring ~ Wikipedia</a></li>
<li><a href="http://www.mathematica-journal.com/2015/11/30/domain-coloring-on-the-riemann-sphere/">Domain coloring on the riemann sphere ~ Mathematica journal</a></li>
<li><a href="http://tools.suckless.org/farbfeld/">farbfeld ~ Suckless</a></li>
</ol>

</body>

</html>
